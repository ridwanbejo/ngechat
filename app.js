var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var swig = require('swig');
var flash = require('express-flash');
var session = require('express-session');
var expressValidator = require('express-validator');
var mongoose = require('mongoose');

var root = require('./routes/index');
var chat_room = require('./routes/chat_room');
var app = express();

// view engine setup
app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname + '/views'));
app.set('view engine', 'html');
app.set('view cache', false);
swig.setDefaults({cache: false});

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));
app.use('/static', express.static(__dirname + '/public'));
app.use(session({secret:"12345"}));
app.use(flash());
app.use(expressValidator());
app.use('/', root);
app.use('/setting/chat_room', chat_room);

// mongoose config
mongoose.connect('mongodb://localhost/ngechat');
mongoose.connection.on('error', function() {
  console.info('Error: Could not connect to MongoDB. Did you forget to run `mongod`?');
});

// socket.io config
var server = require('http').createServer(app);  
var socketIO = require('socket.io')(server);
var Chat = require('./models/chat.js');
var ChatRoom = require('./models/chat_room.js');
var onlineUsers = {};

socketIO.sockets.on('connection', function(socket){
    /*
        daftarkan socket id users ke users_list
        check room apakah yang dituju oleh pengirim pesan
        kirimkan pesan ke semua orang yang ada di room tujuan
    */

    socket.on('joinChat', function(data){
        console.log('join chat...');
        socket.username = data.name;
        onlineUsers[data.name] = socket.id;
    });

    socket.on('sendChat', function(data){
        // check room apakah yang dituju oleh pengirim pesan
        room = ChatRoom.findOne({'name':data.chatRoomID});
        room.exec(function(err, rows){
            var members = rows.members;
            for (var i = 0; i <  members.length; i ++){
                member = members[i];
                if (member != data.sender)
                {
                    socket_id = onlineUsers[member];
                    socketIO.to(socket_id).emit('receiveChat', data);
                }
            }
        });
        
        var message = new Chat({
            sender: data.sender,
            msgContent: data.msgContent,
            chatRoomID: data.chatRoomID,
        });

        message.save(function (err) {
          if (err) return console.error(err);
        });

    });

    socket.on('disconnect', function(data){
        delete onlineUsers[socket.username];
    });

});

server.listen(3030, function(){
    console.log('websocket listening on port 3030...');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
 
// production error handler
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
